package com.book;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.book.entity.Booklab;
import com.book.repository.BooklabRepository;
import com.book.service.Booklabservice;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BooklabApplicationTests {
	
	@Autowired
	private BooklabRepository booklabRepository;
	
	@MockBean
	private Booklabservice booklabservice;
	
	@Test
	public void addBookTest() {
		Booklab booklab = Booklab.builder()
				.bookId(1).bookName("WeCan").bookType("boaring").bookAuthor("mubeen").build();
		booklabRepository.save(booklab);
		Assertions.assertThat(booklab.getBookId()).isGreaterThan(0);
	}

}
