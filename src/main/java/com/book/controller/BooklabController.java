package com.book.controller;
import com.book.entity.Booklab;
import com.book.service.Booklabservice;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BooklabController {
	@Autowired private Booklabservice booklabservice;
	
	// Save operation
	@PostMapping("/booklab")
	public Booklab saveBooklab(@Valid @RequestBody Booklab booklab)
	{
		return booklabservice.saveBooklab(booklab);
	}
	
	// Read operation
	@GetMapping("/booklab")
	public List<Booklab> fetchBooklabList()
	{
		return booklabservice.fetchBooklabList();
	}
	
	@GetMapping("/booklab/{id}")
	public ResponseEntity<Booklab> getBooklabById(@PathVariable("id") Long bookId){
		Booklab booklab = booklabservice.getBooklabById(bookId);
		return ResponseEntity.ok(booklab);
	}

	// Update operation
	@PutMapping("/booklab/{id}")
	public Booklab updateBooklab(@RequestBody Booklab booklab, @PathVariable("id") Long bookId)
	{
		return booklabservice.updateBooklab(booklab, bookId);
	}

	// Delete operation
	@DeleteMapping("/booklab/{id}")
	public String deleteBooklabById(@PathVariable("id") Long bookId)
	{
		booklabservice.deleteBooklabById(bookId);
		return "Deleted Successfully";
	}
}

