package com.book.service;
import com.book.entity.Booklab;


import java.util.List;

public interface Booklabservice {

		// Save operation
		Booklab saveBooklab(Booklab booklab);

		// Read operation
		List<Booklab> fetchBooklabList();

		// Update operation
		Booklab updateBooklab(Booklab booklab, Long booklabId);

		// Delete operation
		void deleteBooklabById(Long booklabId);


		Booklab getBooklabById(Long bookId);



	}


