package com.book.service;

import com.book.entity.Booklab;
import com.book.repository.BooklabRepository;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service

public class BooklabImpl implements Booklabservice {
	@Autowired
	private BooklabRepository booklabRepository;
	
	// Save operation
	@Override
	public Booklab saveBooklab(Booklab booklab)
	{
		return booklabRepository.save(booklab);
	}

	// Read operation
	@Override 
	public List<Booklab> fetchBooklabList()
	{
		return (List<Booklab>) booklabRepository.findAll();
	}
	public Booklab getBooklabById(Long bookId) {
		return booklabRepository.findById(bookId).get();
	}

	// Update operation
	@Override
	public Booklab updateBooklab(Booklab booklab, Long booklabId)
	{
		Booklab depDB = booklabRepository.findById(booklabId).get();
		if (Objects.nonNull(booklab.getBookName())
			&& !"".equalsIgnoreCase(booklab.getBookName())) 
		{
			depDB.setBookName(booklab.getBookName());
		}
		if (Objects.nonNull(booklab.getBookType())
			&& !"".equalsIgnoreCase(booklab.getBookType())) 
		{
			depDB.setBookType(booklab.getBookType());
		}
		if (Objects.nonNull(booklab.getBookAuthor())
			&& !"".equalsIgnoreCase(booklab.getBookAuthor())) {
			depDB.setBookAuthor(booklab.getBookAuthor());
		}
		return booklabRepository.save(depDB);
	}
	// Delete operation
	@Override
	public void deleteBooklabById(Long bookId)
	{
		booklabRepository.deleteById(bookId);
	}
}
