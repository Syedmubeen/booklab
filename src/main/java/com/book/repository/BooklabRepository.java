package com.book.repository;

import com.book.entity.Booklab;
import org.springframework.data.repository.CrudRepository;
public interface BooklabRepository extends CrudRepository<Booklab, Long> {

}
